#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "mem.h"
#include "mem_internals.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  abort();
}


extern inline size_t size_max( size_t x, size_t y );

void free_all(void* start) {
    struct block_header* header = start;
    do {
        _free(header->contents);
    } while ((header = header->next) != NULL);
    _free((uint8_t*)start + offsetof(struct block_header, contents));
}
