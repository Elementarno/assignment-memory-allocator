#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

void test_alloc();
void test_free_block();
void test_free_few_blocks();
void test_expand_heap();
void test_cant_expand_heap();

#endif