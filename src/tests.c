#include "mem_internals.h"
#include "mem.h"

void test_alloc() {
    _malloc(10); // -> 24
    _malloc(100);
    _malloc(1000);
    _malloc(10000);
}

void test_free_block() {
    _malloc(500);
    _malloc(600);
    void* a = _malloc(700);
    _malloc(800);
    _free(a);
}

void test_free_few_blocks() {
    _malloc(200);
    void* a = _malloc(300);
    _malloc(50);
    void* b = _malloc(400);
    void* c = _malloc(500);
    _malloc(1000);
    _malloc(1200);
    _free(c);
    _free(b);
    _free(a);
}

void test_expand_heap() {
    _malloc(2000);
    _malloc(20000);
    _malloc(8000);

}

// Can't create such test
void test_cant_expand_heap() {
    _malloc(10000000);
    _malloc(10000001);
}