#include "mem.h"
#include "util.h"
#include "mem_internals.h"
#include "tests.h"

#define TEST(TEST_FUNCTION) \
        fprintf(stdout, "\nTest #%zu\n", test_num++); \
        TEST_FUNCTION();                           \
        debug_heap(stdout, heap);                  \
        free_all(heap);

int main() {
    size_t test_num = 1;
    void* heap = heap_init(REGION_MIN_SIZE);

    TEST(test_alloc)
    TEST(test_free_block)
    TEST(test_free_few_blocks)
    TEST(test_expand_heap)
    TEST(test_cant_expand_heap)

    return 0;
}
